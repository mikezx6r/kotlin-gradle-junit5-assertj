package ca.mikewilkes

import kotlin.reflect.KProperty

class Delegate {
    private var p: String = "TEST"

    operator fun getValue(foo: Foo, property: KProperty<*>): String {
        return p
    }

    operator fun setValue(foo: Foo, property: KProperty<*>, value: String) {
        p = value + "A"
    }

}

class Foo {
    var p: String by Delegate()
}
