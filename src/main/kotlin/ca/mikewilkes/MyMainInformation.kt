package ca.mikewilkes

interface SomeProduct {
    fun doSomething(parm1: String, parm2: Int): String
    fun somethingElse(parm1:String):String
}

class Productizer : SomeProduct {
    override fun somethingElse(parm1: String): String {
        TODO("not implemented")
    }

    override fun doSomething(parm1: String, parm2: Int): String {
        TODO("not implemented")
    }
}
