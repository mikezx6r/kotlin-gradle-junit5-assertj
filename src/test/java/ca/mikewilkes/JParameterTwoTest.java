package ca.mikewilkes;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class JParameterTwoTest {
    private static Stream<String> single() {
        return Stream.of("foo", "bar");
    }

    private static Stream<Arguments> multi() {
        return Stream.of(
                Arguments.of("foo", 1),
                Arguments.of("Bar", 2)
        );
    }

    @ParameterizedTest
    @MethodSource("single")
    void testWithParameter(String parm1) {
        assertThat(parm1).isNotNull();
    }

    @ParameterizedTest(name = "[{index}] first:{0} - second:{1} ")
    @MethodSource("multi")
    void testWithMultipleParameters(String first, int second) {
        assertThat(first).isNotNull();
        assertThat(second).isNotEqualTo(0);

    }

    @ParameterizedTest
    @CsvSource({"foo, 1", "bar, 2", "'baz, qux', 3"})
    void csvSource(String first, int second) {
        assertThat(first).isNotNull();
        assertThat(second).isNotEqualTo(0);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "two-column.csv")
    void csvFileSource(String first, int second) {
        assertThat(first).isNotNull();
        assertThat(second).isNotEqualTo(0);
    }

    @ParameterizedTest
    @ArgumentsSource(JArgumentsProvider.class)
    void testWithArgumentProviderInJava(String first, int second) {
        assertThat(first).isNotNull();
        assertThat(second).isNotEqualTo(0);
    }

    @ParameterizedTest
    @ArgumentsSource(KArgumentsProvider.class)
    void testWithArgumentProviderInKotlin(String first, int second) {
        assertThat(first).isNotNull();
        assertThat(second).isNotEqualTo(0);
    }
}
