package ca.mikewilkes;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

class JFirstTest {
    @Test
    void firstFailingTest() {
        assertThat(2).isEqualTo(5);
    }

    @Test
    @DisplayName("Verify Equals and Display name work")
    void secondTest() {
        assertThat(12).isEqualTo(12);
    }

    @Test
    @Disabled
    void aDisabledTest() {
        fail("Fail if we reach here");
    }

    @ParameterizedTest(name = "({index}) Parameterized test that takes an int. number={0}")
    @ValueSource(ints = {1, 2, 3})
    void thirdTest(int input) {
        assertThat(2).isEqualTo(input);
    }
}
