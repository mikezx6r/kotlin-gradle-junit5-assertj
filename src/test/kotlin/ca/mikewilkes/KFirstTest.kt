package ca.mikewilkes

import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class KFirstTest {

    @Test
    fun firstFailingTest() {
        2 isEqualTo 5
    }

    @Test
    @DisplayName("Verify Equals and Display name work")
    fun secondTest() {
        3 isEqualTo 3
    }

    @Test
    @Disabled
    fun aDisabledTest() {
        fail<Unit>("Fail if we reach here")
    }

    @ParameterizedTest(name = "Simple parameterized test taking a single int: ({index}) number: {0}")
    @ValueSource(ints = [1, 2])
    fun thirdTest(input: Int) {
        2 isEqualTo input
    }

    @Test
    fun testDelegate() {
        val foo = Foo()

        foo.p isEqualTo "TEST"

        foo.p = "Some Other Value"
        foo.p isEqualTo "Some Other ValueA"
    }
}
