package ca.mikewilkes

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsSource
import org.junit.jupiter.params.provider.CsvFileSource
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class KParameterTwoTest {
    @ParameterizedTest
    @MethodSource("single")
    fun testWithParameter(parm1: String) {
        parm1.isNotNull()
    }

    @ParameterizedTest(name = "[{index}] first:{0} - second:{1} ")
    @MethodSource("multi")
    fun testWithMultipleParameters(first: String, second: Int) {
        first.isNotNull()
        second isNotEqualTo 0

    }

    @ParameterizedTest(name = "Some name [{index}] {0}, {1}")
    @CsvSource("foo, 1", "bar, 2", "'baz, qux', 3")
    fun csvSource(first: String, second: Int) {
        first.isNotNull()
                .isNotEqualTo("test")

        second isNotEqualTo 0
                .isNotNull()
    }

    @ParameterizedTest
    @CsvFileSource(resources = ["two-column.csv"])
    fun csvFileSource(first: String, second: Int) {
        first.isNotNull()
        second isNotEqualTo 0
    }

    @ParameterizedTest
    @ArgumentsSource(JArgumentsProvider::class)
    fun testWithArgumentProviderInJava(first: String, second: Int) {
        first.isNotNull()
        second isNotEqualTo 0
    }

    @ParameterizedTest
    @ArgumentsSource(KArgumentsProvider::class)
    fun testWithArgumentProviderInKotlin(first: String, second: Int) {
        first.isNotNull()
        second isNotEqualTo 0
    }

    companion object {
        @Suppress("unused")
        @JvmStatic
        fun multi(): Stream<Arguments> {
            return Stream.of<Arguments>(
                    Arguments.of("foo", 1),
                    Arguments.of("Bar", 2)
            )
        }

        @Suppress("unused")
        @JvmStatic
        fun single(): Stream<String> {
            return Stream.of("foo", "bar")
        }
    }
}
