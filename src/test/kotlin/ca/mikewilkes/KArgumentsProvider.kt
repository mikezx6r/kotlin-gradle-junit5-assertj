package ca.mikewilkes

import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import java.util.stream.Stream

class KArgumentsProvider : ArgumentsProvider {

    override fun provideArguments(context: ExtensionContext): Stream<out Arguments> = Stream.of<Arguments>(
            Arguments.of("foo1", 1),
            Arguments.of("foo2", 2)
    )
}
