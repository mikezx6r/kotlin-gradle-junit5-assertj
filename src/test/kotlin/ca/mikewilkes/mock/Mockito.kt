package ca.mikewilkes.mock

import ca.mikewilkes.SomeProduct
import ca.mikewilkes.isEqualTo
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.mockito.junit.jupiter.MockitoSettings
import org.mockito.quality.Strictness

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class Mockito {

    private val mine: SomeProduct = mock(verboseLogging = true, stubOnly = false)

    @Test
    fun someTest() {
        whenever(mine.doSomething("a", 1)).thenReturn("A")
        whenever(mine.somethingElse("a")).thenReturn("A")

        mine.doSomething("a", 1) isEqualTo "A"

        whenever(mine.doSomething("b", 2)).thenReturn("B")
        mine.doSomething("b", 2) isEqualTo "B"

        // fails on null because no mock provided BUT returning null even though it should be strict
//        mine.doSomething("c", 5) isEqualTo "C"

        mine.doSomething("c", 2)

//        mine.somethingElse("B")
    }
}
