package ca.mikewilkes.mock

import ca.mikewilkes.SomeProduct
import ca.mikewilkes.isEqualTo
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Mockk2 {
    val mine = mockk<SomeProduct>()

    // Reports very accurately if no matching mock found even if one on that method already found
    // Doesn't report unnecessary mocking
    // Relatively slow
    @Test
    fun someTest() {
        every { mine.doSomething("a", 1) } returns "A"
        every { mine.doSomething("b", 2) } returns "B"

        mine.doSomething("a", 1) isEqualTo "A"
        mine.doSomething("b", 2) isEqualTo "B"

//        every {mine.somethingElse("A")} returns "B"

//        no answer found for: SomeProduct(#1).doSomething(c, 5)
//        io.mockk.MockKException: no answer found for: SomeProduct(#1).doSomething(c, 5)
//        mine.doSomething("c", 5) isEqualTo "C"
    }
}
