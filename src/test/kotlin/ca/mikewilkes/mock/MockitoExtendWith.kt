package ca.mikewilkes.mock

import ca.mikewilkes.SomeProduct
import ca.mikewilkes.isEqualTo
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.junit.jupiter.MockitoSettings
import org.mockito.quality.Strictness

@ExtendWith(MockitoExtension::class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class MockitoExtendWith {

    // Mockito detects unnecessary stubbings
    // Doesn't allow easy overriding of previous errors
    // Doesn't report very nicely if a stub is called w/no values even when strict
    @Mock
    lateinit var mine: SomeProduct

    @Test
    fun someTest() {
        whenever(mine.doSomething("a", 1)).thenReturn("A")
//        whenever(mine.somethingElse("a")).thenReturn("A")

        mine.doSomething("a", 1) isEqualTo "A"

        whenever(mine.doSomething("b", 2)).thenReturn("B")
        mine.doSomething("b", 2) isEqualTo "B"

        // fails on null because no mock provided BUT returning null even though it should be strict
//        mine.doSomething("c", 5) isEqualTo "C"

        mine.doSomething("c", 2)

        mine.somethingElse("B")
    }
}
