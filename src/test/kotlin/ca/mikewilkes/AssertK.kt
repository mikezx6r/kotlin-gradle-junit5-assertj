package ca.mikewilkes

import org.assertj.core.api.AbstractObjectAssert
import org.assertj.core.api.Assertions.assertThat

infix fun <T : Any> T.isEqualTo(parm: T): AbstractObjectAssert<*, *> = assertThat(this).isEqualTo(parm)
infix fun <T : Any> T.isNotEqualTo(parm: T): AbstractObjectAssert<*, *> = assertThat(this).isNotEqualTo(parm)
fun <T : Any?> T.isNotNull(): AbstractObjectAssert<*, *> = assertThat(this).isNotNull

