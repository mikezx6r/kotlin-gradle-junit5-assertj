package ca.mikewilkes.mock2

import ca.mikewilkes.SomeProduct
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Mockk {
    private val mine: SomeProduct = mockk()
    private val mineRelaxed: SomeProduct = mockk(relaxed = true)

    @Test
    fun mockkTest() {
        mine.doSomething("a", 1)
    }

    @Test
    fun mockkRelaxed() {
        mineRelaxed.doSomething("a", 1)
    }
}
