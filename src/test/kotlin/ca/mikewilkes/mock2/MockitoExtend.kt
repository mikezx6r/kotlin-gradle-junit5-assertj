package ca.mikewilkes.mock2

import ca.mikewilkes.SomeProduct
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class MockitoExtend {
    @Mock
    private lateinit var mine: SomeProduct

    @Test
    fun mockitoExtendTest() {
        whenever(mine.doSomething("B", 2)).thenReturn("B")

        mine.doSomething("a", 1)
    }
}
