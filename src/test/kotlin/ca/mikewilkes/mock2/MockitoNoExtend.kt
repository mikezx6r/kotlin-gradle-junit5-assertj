package ca.mikewilkes.mock2

import ca.mikewilkes.SomeProduct
import ca.mikewilkes.isNotNull
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MockitoNoExtend {
    private val mine: SomeProduct = mock()

    @Test
    fun mockitoNoExtend1() {
        whenever(mine.doSomething("B", 2)).thenReturn("B")

        val doSomething = mine.doSomething("a", 1)

        doSomething.isNotNull()
    }
}

