# Testing with JUnit 5
## Overview

Sample project using Gradle, Kotlin, JUnit5 and AssertJ.

Implements various tests to demonstrate some of the JUnit5 features, and to contrast usage differences between Java and Kotlin.

There are several test failures coded to demonstrate the error reporting facilities

- [JUnit5 User Guide](http://junit.org/junit5/docs/current/user-guide/)
- [AssertJ](https://joel-costigliola.github.io/assertj/index.html)


## Kotlin and Intellij shortcomings

### Parameterized Test using Method Source
Using Java, IntelliJ correctly detects parameter test method usage, and will navigate to definitions.
When using Kotlin, it thinks the method is unused.

### Annotation values
In Kotlin, one must define an arrayOf for an Annotation parameter, even if one only
wants to pass a single parameter.
